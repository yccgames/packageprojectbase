using System;
using UnityEngine;

namespace BehaviourByStages
{
   [RequireComponent(typeof(AbstractStagesBehavioursController<>))]
   public abstract class AbstractStageBehaviour<T> : MonoBehaviour where T : Enum
   {
      [SerializeField]
      protected T stage;

      [SerializeField]
      private float stageWeight;

      private float actualStageWeight;

      protected AbstractStagesBehavioursController<T> RefStagesBehavioursController;

      public T Stage
         => stage;

      public float ActualStageWeight
      {
         get => actualStageWeight;
         set => actualStageWeight = value;
      }

      public float StageWeight
         => stageWeight;

      public void SetupWeight()
      {
         actualStageWeight = stageWeight;
      }

      public bool HasStageAnd(params Enum[] argStagesForEvaluating)
      {
         GetReferenceStagesBehavioursController();
         return RefStagesBehavioursController.HasStageAnd(argStagesForEvaluating);
      }

      public bool HasStageOr(params Enum[] argStagesForEvaluating)
      {
         GetReferenceStagesBehavioursController();
         return RefStagesBehavioursController.HasStageOr(argStagesForEvaluating);
      }

      public void AddStage(Enum argNewStage)
      {
         GetReferenceStagesBehavioursController();
         RefStagesBehavioursController.AddStage(argNewStage);
      }

      public void AddStage(params Enum[] argNewStages)
      {
         GetReferenceStagesBehavioursController();
         RefStagesBehavioursController.AddStage(argNewStages);
      }

      public void RemoveStage(Enum argNewStage)
      {
         GetReferenceStagesBehavioursController();
         RefStagesBehavioursController.RemoveStage(argNewStage);
      }

      public void RemoveStage(params Enum[] argNewStages)
      {
         GetReferenceStagesBehavioursController();
         RefStagesBehavioursController.RemoveStage(argNewStages);
      }

      public void RemoveAllStages()
      {
         GetReferenceStagesBehavioursController();
         RefStagesBehavioursController.RemoveAllStages();
      }

      public void SetStage(Enum argNewStage)
      {
         GetReferenceStagesBehavioursController();
         RefStagesBehavioursController.SetStage(argNewStage);
      }

      /// <summary>
      /// Si tiene el stage lo quita, si no lo tiene lo pone
      /// </summary>
      /// <param name="argStage">Stage para cambiar el estado</param>
      public void ToggleStage(Enum argStage)
      {
         GetReferenceStagesBehavioursController();
         RefStagesBehavioursController.ToggleStage(argStage);
      }

      private void GetReferenceStagesBehavioursController()
      {
         if(!RefStagesBehavioursController)
            RefStagesBehavioursController = GetComponent<AbstractStagesBehavioursController<T>>();
      }
   }
}