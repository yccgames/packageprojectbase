using System;
using System.Collections.Generic;
using UnityEngine;

namespace Patterns.Observer
{
   public abstract class AbstractSubject : MonoBehaviour, ISubject
   {
      private readonly List<IObserver> listObservers = new List<IObserver>();

      public void SuscribeObserver(IObserver argIObserver)
      {
         listObservers.Add(argIObserver);
      }

      public void UnsuscribeObserver(IObserver argIObserver)
      {
         listObservers.Remove(argIObserver);
      }

      public void NotifyToObservers(Enum argFlag)
      {
         foreach(var tmpObserver in listObservers)
            tmpObserver.UpdateFromSubject(argFlag);
      }
   }
}